document.write("Dispalying the text from some other external file.");
document.write("<p><i><u>A. Expalining the operators</u></i></p>");

var a = 10;
var b = 20;
var c = "Kashish";
var result = a + b + c + a + b;

document.write("<p>Result is : " + result + ".</p>");

//-------------------------------------------------------- IF - ELSE -------------------------------------------------------------------
/*
1) Find whether the number is even or odd.
2) Find whether the number is positive, negative , or 0.
3) Find whetehr the number is positive and even.
*/

document.write("<i><u>B. If - else statements : </u></i>");

//1
document.write("<h4>1.Find whether the number is even or odd.</h4>");
var x = 1;

if (x % 2 == 0) {
    document.write("The number : " + x + " is Even.");

} else {
    document.write("The number : " + x + " is Odd.");

}

//2
document.write("<h4>2. Find whether the number is positive, negative , or 0.</h4>");
var x = -1;

if (x > 0) {
    document.write("The number : " + x + " is Positive.");

} else if (x < 0) {
    document.write("The number : " + x + " is Negative.");

}
else {
    document.write("The number : " + x + " is 0.");

}

//3
document.write("<h4>3. Find whetehr the number is positive and even..</h4>");
var x = 100;

if ((x > 0) && (x % 2 == 0)) {
    document.write("The number : " + x + " is Positive and even.");

}
else {
    document.write("The number : " + x + " is not positive and even.");

}

//-------------------------------------------------------- SWITCH STATEMENTS -------------------------------------------------------------------
document.write("<p><i><u>C. Switch statements : </u></i></p>");

document.write("<p><b>Switch statements </b>: Generally preffered when there are n number of cases, for faster execution as well as clean code</p>");
document.write("<p>JS Switch statemnets are different from java/php or any other language : 1. var can be either string, number or boolean.  2. In switch conditional brackets, operators can be performed. eg. switch (day + 1)</p>");

/*Find day of week by accepting its number.
eg 1 -> Sunday, 2-> Monday etc
*/

//4
document.write("<h4>4. Find day of week by accepting its number.</h4>");

var day = 49;

switch (day) {
    case 1:
        document.write("Day : " + day + " is Monday.");
        break;

    case 2:
        document.write("Day : " + day + " is Tuesday.");
        break;

    case 3:
        document.write("Day : " + day + " is Wednesday.");
        break;

    case 4:
        document.write("Day : " + day + " is Thursday.");
        break;

    case 5:
        document.write("Day : " + day + " is Friday.");
        break;

    case 6:
        document.write("Day : " + day + " is Saturday.");
        break;

    case 7:
        document.write("Day : " + day + " is Sunday.");
        break;

    default:
        document.write("Wrong Input.");


}

//-------------------------------------------------------- FOR LOOPS -------------------------------------------------------------------
document.write("<p><i><u>D. For Loops statements :</u></i></p>");

document.write("<p> For Loops is used when number of iterations are known in advance. ENTRY CONTROLLED LOOP - First condition is checked and the loop</p>");

/*
1. Print tables of 5 using for loop till 10 iterations.
2. Print first 5 even numbers.2, 4, 6, 8, 10
*/

//for (int; condition; increment/decrement){}

//1
document.write("<h4><p>1. Print tables of 5 using for loop till 10 iterations.</p></h4>");

for (var x = 1; x <= 10; x++) {
    document.write("<p> 5*" + x + "= " + (5 * x) + "</p>");

}

//2
document.write("<p><h4>2. Print first 5 even numbers.</h4></p>");

for (var k = 2; k <= 10; k += 2) {
    document.write("<p>" + k + "</p>");

}

//-------------------------------------------------------- WHILE LOOPS -------------------------------------------------------------------
document.write("<p><i><u>E. While Loops statements :</u></i></p>");

document.write("<p> while Loops is used when number of iterations are unknown in advance. ENTRY CONTROLLED LOOP - First condition is checked and the loop</p>");

//while() {}

// Print first 5 odd numbers

document.write("<p><h4>1. Print first 5 odd numbers.</h4></p>");

var x = 1;
while (x <= 10) {
    document.write("<p>" + x + "</p>");
    x = x + 2;

}

//-------------------------------------------------------- DO-WHILE LOOPS -------------------------------------------------------------------
document.write("<p><i><u>E. DO-While Loops statements :</u></i></p>");

document.write("<p>EXIT  CONTROLLED LOOP - First statement executed, then the condition is checked and the loop</p>");

/*do{

}
while();
*/

document.write("<p><h4>1. Example to show usage of Do while loop.</h4></p>");

var x = 10;

do {

    document.writeln("Kashish");
    x--;

} while (x > 5);


//-------------------------------------------------------- FUNCTIONS -------------------------------------------------------------------
document.write("<p><i><u>F. Functions :</u></i></p>");

// document.write("<p> </p>");

// function abcXyz()

document.write("<p><h4>1. Write the function to add 2 numbers and print the result.</h4></p>");

function addNumbers(a, b) {
    var sum = a + b
    document.writeln("The sum of " + a + " and " + b + " is " + sum);
    return sum;

}

var a = 10;
var b = 12;

var output = addNumbers(a, b);

document.writeln("<p> OUTPUT  >>> " + output + "</p>");

//-------------------------------------------------------- ARRAY -------------------------------------------------------------------
document.write("<p><i><u>G. ARRAY :</u></i></p>");

document.write("<p> JS ARRAY is different from other langauage array. 1. Dynamic in nature 2. Can store different data types elemnet </p>");

// var array = ["kashish", "Chaurasia" , "1"]; (Recommended)
//var array = new array ("Kashish","Chaurasia","1")

document.write("<p><h4>1. To demonstrate the JS Array working.</h4></p>");

var carsArray = ["BMW", "Mercedes", "Volvo"];
//Append array
carsArray.push("Zantro");

for (var i = 0; i <= carsArray.length - 1; i++) {
    document.writeln(carsArray[i]);
}

carsArray.push(1); // pushing element int : '1' in the array

document.writeln("<p>The element at position 3 :</p> " + carsArray[3]);

document.writeln("<p>The element at position 5 :</p> " + carsArray[4]);

//____________________________________

//Remove From the Front of an Array
var a = ['first', 'second', 'third'];
document.writeln('Original Array:', a);

// Remove the first element from the array
let removed = a.shift();

document.writeln('Modified Array:', a);
document.writeln('Removed Element:', removed);

//____________________________________

//Add to the Front of an Array
var a = ['first', 'second', 'third'];
document.writeln('Original Array:', a);

// Insert element at the beginning of the array
a.unshift('fourth');

document.writeln('Modified Array:', a);

//____________________________________

// Append to the End of an Array
var a = ['first', 'second'];

// Append 'third' to array 'a'
a.push('third');

document.writeln('a:', a);

//_______________________________________

// Remove From the end of an Array
var a = ['first', 'second', 'third'];
document.writeln('Original Array:', a);

// Remove the last element from the array
let removed_end = a.pop();

document.writeln('Modified Array:', a);
document.writeln('Removed Element:', removed_end);

//_________________________________________

//Add to the Front of an Array
var a = ['first', 'second', 'third'];
document.writeln('Original Array:', a);

// Insert element at the beginning of the array
a.unshift('fourth');

document.writeln('Modified Array:', a);

//__________________________________________

//Find the Index of an Item in the Array

var a = ['first', 'second', 'third', 'fourth'];

let position = a.indexOf('second');

document.writeln('a:', a);
document.writeln('position:', position);

//___________________________________________

//Remove an Item by Index Position

var a = ['first', 'second', 'third', 'fourth', 'fifth'];
document.writeln('Original Array:', a);

let position_index = 1;
let elementsToRemove = 2;
// Remove 'elementsToRemove' element(s) starting at 'position'
a.splice(position_index, elementsToRemove);

document.writeln('Modified Array:', a);

//_____________________________________________

//Copy an Array
var a = ['first', 'second', 'third', 'fourth'];
document.writeln('a:', a);

// Shallow copy array 'a' into a new object
let b_copy = a.slice();

document.writeln('b:', b_copy);

//_____________________________________________

//Sort anr Array
var a = ['c', 'a', 'd', 'b', 'aa'];
var b = [9, 2, 13, 7, 1, 12, 123];

// Sort in ascending lexicographical order using a built-in
a.sort();
b.sort();

document.writeln('a:', a);
document.writeln('b:', b);



//-------------------------------------------------------- OBJECTS -------------------------------------------------------------------
document.write("<p><i><u>H. OBJECTS :</u></i></p>");

document.write("<p> IMPORTATNT CONCEPTS : </p>");

//Method 1
var Car = { // car is an object

    car_brand: "Tesla",
    car_model: "Model 1",
    car_price: 3500000,
    auto_pilot: function () {
        document.writeln("This model have auto pilot feature ! :)");
    }
}

document.writeln("Car Model : " + Car.car_model);
Car.auto_pilot();

//Method 2

function Cars(car_brand, car_model, car_price) {
    this.car_brand = car_brand;
    this.car_model = car_model;
    this.car_price = car_price;

    this.autoPilot = function () {
        document.writeln("This model have auto pilot feature ! :)");
    }
}

var car1 = new Cars("Tesla", "model 2", 3500000); // car1 -> object 1
var car2 = new Cars("Tesla", "modle 1", 250000); // car2 -> object 2 

document.writeln(car1.car_brand + car1.car_model + car1.car_price);
car1.autoPilot();

var a = "kashish" // data type of 'a' is string
var b = new String() // data type of 'b' is Object 

typeof (b); //object

Car.car_fueltype = "Electric";  // if you forget to add one more property, after 100 lines of code

Car.newFunction = function () {
    return " Hello, I am new !";
}
document.writeln(Car.car_fueltype);
document.writeln(Car.newFunction());

delete Car.car_fueltype; // delete particular property of the object
document.writeln(Car.car_fueltype); // undefined

//-------------------------------------------------------- LET & CONST KEYWORDS -------------------------------------------------------------------
document.write("<p><i><u>I. LET & CONST:</u></i></p>");

/*
LET
We use the let keyword to declare variables that are limited in scope to the block, statement, or expression in which they are used. 
This is unlike the var keyword, which defines a variable globally or locally to an entire function regardless of block scope.

CONST

We use the const keyword to create a read-only reference to a value, meaning the value referenced by this variable cannot be reassigned. 
Because the value referenced by a constant variable cannot be reassigned,JavaScript requires that constant variables always be initialized.

*/

var a = 100;
var b = 10;
document.writeln(b);

if (b == 10) {
    let a = 300; //
    document.writeln(a);
}
else {
    document.writeln(a);
}

document.writeln(a);

const variable = 9;
document.writeln(variable);

//-------------------------------------------------------- DOM MANIPULATIONS -------------------------------------------------------------------
document.write("<p><i><u>J. Dom Manipulations:</u></i></p>");

function buttonClick() {
    alert("Button is clicked !");

}

function buttonClick2() {
    //document get elemrnt by ID
    document.getElementById("heading2").innerHTML = "Kashish is learning :)";
}

function buttonClick3() {
    //document get elemrnt by ID and Input Tag
    var username = document.getElementById("input1").value;
    var password = document.getElementById("input2").value;

    if (username == password) {
        alert("Its a Match !");
    } else {
        alert("Username and Password doesn't Match :(");
    }
}

function buttonClick4() {
    //document get element by ID and Input Tag - Radio Buttons
    var rd1 = document.getElementById("rd1");
    var rd2 = document.getElementById("rd2");
    var rd3 = document.getElementById("rd3");

    if (rd1.checked == true) {
        alert("You are Beautiful !");
    } else if (rd2.checked == true) {
        alert("You are Handsome !");

    } else if (rd3.checked == true) {
        alert("You are Blessed !");

    } else {
        alert("Come Dude, Select Something !");
    }

}

function buttonClick5() {
    //document get element by ID and Input Tag - Select DropDown
    var select = document.getElementById("selectBox1");
    // alert(select.options[select.selectedIndex].value);
    alert(select.options[select.selectedIndex].value);
}

function buttonClick6() {
    //document get element by tagName  and className
    var para = document.getElementsByTagName("p");
    para[para.length-4].style.color = "red";
    para[para.length-3].style.fontWeight = "bold";
    para[para.length-2].style.fontStyle = "italic";
    para[para.length-1].style.color = "blue";

    for(var i = 0; i<para.length; i++){
        para[i].style.fontSize = 20;

    }

    var classPara = document.getElementsByClassName("kashish");
    for(var i = 0; i<classPara.length; i++){
        classPara[i].style.fontSize = 15;

    }

}
















